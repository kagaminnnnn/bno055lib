#include "bno055.hpp"

namespace Bno055Lib{
  void Bno055::Setup(){
    Wire.begin(sda_,scl_);
    Wire.setClock(400000); // I2C clock rate ,You can delete it but it helps the speed of I2C (default rate is 100000 Hz)
    delay(10);
    Wire.beginTransmission(kGY955Address);
    Wire.write(0x3E); // Power Mode
    Wire.write(0x00); // Normal:0X00 (or B00), Low Power: 0X01 (or B01) , Suspend Mode: 0X02 (orB10)
    Wire.endTransmission();
    delay(10);
    Wire.beginTransmission(kGY955Address);
    Wire.write(0x3D); // Operation Mode
    Wire.write(0x0C); //NDOF:0X0C (or B1100) , IMU:0x08 (or B1000) , NDOF_FMC_OFF: 0x0B (or B1011)
    Wire.endTransmission();
    delay(10);
  }

  void Bno055::Update(){
    Wire.beginTransmission(kGY955Address);
    Wire.write(0x08);  
    Wire.endTransmission(false);
    Wire.requestFrom(kGY955Address,32,true);

    // Accelerometer
    accx_ = static_cast<int16_t>(Wire.read() | Wire.read() << 8) / 100.00; // m/s^2
    accy_ = static_cast<int16_t>(Wire.read() | Wire.read() << 8) / 100.00; // m/s^2
    accz_ = static_cast<int16_t>(Wire.read() | Wire.read() << 8) / 100.00; // m/s^2
    // Magnetometer
    magx_ = static_cast<int16_t>(Wire.read() | Wire.read() << 8) / 16.00; // mT
    magy_ = static_cast<int16_t>(Wire.read() | Wire.read() << 8) / 16.00; // mT
    magz_ = static_cast<int16_t>(Wire.read() | Wire.read() << 8) / 16.00; // mT
    // Gyroscope
    gyrox_ = static_cast<int16_t>(Wire.read() | Wire.read() << 8) / 16.00; // Dps
    gyroy_ = static_cast<int16_t>(Wire.read() | Wire.read() << 8) / 16.00; // Dps
    gyroz_ = static_cast<int16_t>(Wire.read() | Wire.read() << 8) / 16.00; // Dps
    // Euler Angles
    yaw_[0] = static_cast<int16_t>(Wire.read() | Wire.read() << 8) / 16.00;  //in Degrees unit
    roll_[0] = static_cast<int16_t>(Wire.read() | Wire.read() << 8) / 16.00;  //in Degrees unit
    pitch_[0] = static_cast<int16_t>(Wire.read() | Wire.read() << 8) / 16.00;  //in Degrees unit
    // Quaternions
    q_[0] = static_cast<int16_t>(Wire.read() | Wire.read() << 8) / (pow(2,14)); //unit less
    q_[1] = static_cast<int16_t>(Wire.read() | Wire.read() << 8) / (pow(2,14)); //unit less
    q_[2] = static_cast<int16_t>(Wire.read() | Wire.read() << 8) / (pow(2,14)); //unit less
    q_[3] = static_cast<int16_t>(Wire.read() | Wire.read() << 8) / (pow(2,14)); //unit less
    //Convert Quaternions to Euler Angles
    yaw_[1] = (atan2(2 * (q_[0] * q_[3] + q_[1] * q_[2]),1 - 2 * (pow(q_[2],2) + pow(q_[3],2)))) * kDegFromRadian;
    roll_[1] = (asin(2 * (q_[0] * q_[2] - q_[3] * q_[1]))) * kDegFromRadian;
    pitch_[1] = (atan2(2*(q_[0] * q_[1] + q_[2] * q_[3]),1 - 2*(pow(q_[1],2) + pow(q_[2],2)))) * kDegFromRadian;
  }
}
