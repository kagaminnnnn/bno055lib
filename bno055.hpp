#ifndef BNO055_LIB
#define BNO055_LIB

#include <Arduino.h>
#include <Wire.h>

namespace Bno055Lib{
  class Bno055{
    static constexpr uint8_t kGY955Address = 0x28;
    static constexpr double kDegFromRadian = 180.0 / PI;
    uint8_t scl_;
    uint8_t sda_;
    double magx_,magy_,magz_,accx_, accy_,accz_, gyrox_,gyroy_,gyroz_;
    double yaw_[2];
    double roll_[2];
    double pitch_[2];
    double q_[4];
  public:
    Bno055(uint8_t scl = 17,uint8_t sda = 16):scl_{scl},sda_{sda}{}
    void Setup();
    void Update();

    const double& GetAccelX()const{return accx_;}
    const double& GetAccelY()const{return accy_;}
    const double& GetAccelZ()const{return accz_;}

    const double& GetMagX()const{return magx_;}
    const double& GetMagY()const{return magy_;}
    const double& GetMagZ()const{return magz_;}

    const double& GetGyroX()const{return gyrox_;}
    const double& GetGyroY()const{return gyroy_;}
    const double& GetGyroZ()const{return gyroz_;}

    const double& GetYaw(uint8_t num = 0)const{
      return yaw_[num];
    }
    const double& GetRoll(uint8_t num = 0)const{
      return roll_[num];
    }
    const double& GetPitch(uint8_t num = 0)const{
      return pitch_[num];
    }

    const double& GetQ(uint8_t num)const{
      return q_[num];
    }    
  };
}


#endif